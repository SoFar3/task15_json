package com.epam;

import com.epam.controller.ControllerImpl;

import java.io.FileNotFoundException;

public class App {

    public static void main(String[] args) throws FileNotFoundException {
        new ControllerImpl().start();
    }

}
