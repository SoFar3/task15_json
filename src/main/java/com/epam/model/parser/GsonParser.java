package com.epam.model.parser;

import com.epam.model.Candy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;

public class GsonParser implements Parser {

    private File file;

    private Gson gson;

    public GsonParser(String fileName) {
        gson = new Gson();
        this.file = new File("src/main/resources/" + fileName);
    }

    @Override
    public List<Candy> parse() {
        List<Candy> candies = null;

        try {
            Type type = new TypeToken<List<Candy>>() {}.getType();
            candies = gson.fromJson(new FileReader(file), type);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return candies;
    }

}
