package com.epam.model.parser;

import com.epam.model.Candy;

import java.util.List;

public interface Parser {

    List<Candy> parse();

}
