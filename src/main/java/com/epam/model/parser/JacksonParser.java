package com.epam.model.parser;

import com.epam.model.Candy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JacksonParser implements Parser {

    private ObjectMapper mapper;

    private File file;

    public JacksonParser(String fileName) {
        this.mapper = new ObjectMapper();
        this.file = new File("src/main/resources/" + fileName);
    }

    @Override
    public List<Candy> parse() {
        List<Candy> candies = null;

        try {
             candies = mapper.readValue(file, new TypeReference<List<Candy>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }

        return candies;
    }

}
