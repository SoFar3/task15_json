package com.epam.model;

public class Value {

    private String albumen;
    private String fat;
    private String carbohydrates;

    public Value() { }

    public Value(String albumen, String fat, String carbohydrates) {
        this.albumen = albumen;
        this.fat = fat;
        this.carbohydrates = carbohydrates;
    }

    public String getAlbumen() {
        return albumen;
    }

    public void setAlbumen(String albumen) {
        this.albumen = albumen;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(String carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "Value{" +
                "albumen='" + albumen + '\'' +
                ", fat='" + fat + '\'' +
                ", carbohydrates='" + carbohydrates + '\'' +
                '}';
    }

}
