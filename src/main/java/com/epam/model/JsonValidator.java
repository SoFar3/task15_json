package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class JsonValidator {

    private static Logger logger = LogManager.getLogger();

    private File json;
    private File schema;

    public JsonValidator(String json, String schema) {
        this.json = new File("src/main/resources/" + json);
        this.schema = new File("src/main/resources/" + schema);
    }

    public boolean validate() throws FileNotFoundException {
        JSONArray jsonData = new JSONArray(
                new JSONTokener(new FileReader(json))
        );
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(new FileReader(schema))
        );
        try {
            Schema schema = SchemaLoader.load(jsonSchema);
            for (int i = 0; i < jsonData.length(); i++) {
                schema.validate(jsonData.get(i));
            }
        } catch (ValidationException e) {
            logger.warn(e.getMessage());
            e.getCausingExceptions().stream()
                    .map(ValidationException::getMessage)
                    .forEach(logger::warn);
            return false;
        }

        return true;
    }

}
