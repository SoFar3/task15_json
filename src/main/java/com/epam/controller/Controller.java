package com.epam.controller;

import java.io.FileNotFoundException;

public interface Controller {

    void start() throws FileNotFoundException;

}
