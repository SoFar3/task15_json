package com.epam.controller;

import com.epam.model.Candy;
import com.epam.model.JsonValidator;
import com.epam.model.parser.GsonParser;
import com.epam.model.parser.JacksonParser;
import com.epam.model.parser.Parser;
import com.epam.view.View;
import com.epam.view.ViewImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.util.List;

public class ControllerImpl implements Controller {

    private static Logger logger = LogManager.getLogger();

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() throws FileNotFoundException {
        String jsonData = "candy.json";
        String jsonSchema = "candyScheme.json";

        JsonValidator jsonValidator = new JsonValidator(jsonData, jsonSchema);

        if (jsonValidator.validate()) {
            Parser jackson = new JacksonParser(jsonData);
            List<Candy> jackCandy = jackson.parse();
            logger.info(jackCandy);

            Parser gson = new GsonParser(jsonData);
            List<Candy> georgeCandy = gson.parse();
            logger.info(georgeCandy);
        }
    }

}
